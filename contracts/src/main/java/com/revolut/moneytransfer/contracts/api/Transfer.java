package com.revolut.moneytransfer.contracts.api;

import java.math.BigDecimal;

public interface Transfer {
    void transfer(String fromAccountId, String toAccountId, BigDecimal amount, String currency, String requestId) throws Exception;
}
