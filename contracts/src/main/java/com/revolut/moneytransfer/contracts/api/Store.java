package com.revolut.moneytransfer.contracts.api;

import com.revolut.moneytransfer.contracts.models.Account;

import java.math.BigDecimal;
import java.util.List;

public interface Store {
    List<Account> getAccounts();

    Account getAccount(String id);

    Account createAccount(String name, String currency);

    void deposit(String account, BigDecimal amount) throws Exception;

    void transfer(String fromId, String toId, BigDecimal amount) throws Exception;
}
