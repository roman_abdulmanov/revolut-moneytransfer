package com.revolut.moneytransfer.contracts.models;

import java.math.BigDecimal;


public interface Account {
    String getId();

    String getName();

    String getCurrency();

    BigDecimal getBalance();
}
