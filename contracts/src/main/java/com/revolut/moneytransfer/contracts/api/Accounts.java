package com.revolut.moneytransfer.contracts.api;

import com.revolut.moneytransfer.contracts.models.Account;

import java.util.List;

public interface Accounts {
    List<Account> getAccounts();

    Account getAccount(String id);

    Account createAccount(String name, String currency, String requestId) throws Exception;
}
