package com.revolut.moneytransfer.contracts.models.dto;

import com.revolut.moneytransfer.contracts.models.Account;

import java.math.BigDecimal;

public class AccountDto implements Account {
    private String _id;
    private String _name;
    private BigDecimal _balance;
    private String _currency;

    public AccountDto() {
    }

    public AccountDto(String id, String name, BigDecimal balance, String currency) {
        _id = id;
        _name = name;
        _balance = balance;
        _currency = currency;
    }

    public String getId() {
        return _id;
    }

    public void setId(String value) {
        _id = value;
    }

    public String getName() {
        return _name;
    }

    public void setName(String value) {
        _name = value;
    }

    public String getCurrency() {
        return _currency;
    }

    public void setCurrency(String value) {
        _currency = value;
    }

    public BigDecimal getBalance() {
        return _balance;
    }

    public void setBalance(BigDecimal value) {
        _balance = value;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "id:'" + _id + '\'' +
                ", name:'" + _name + '\'' +
                ", balance:" + _balance +
                ", currency:'" + _currency + '\'' +
                '}';
    }
}
