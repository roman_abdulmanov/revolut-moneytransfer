package com.revolut.moneytransfer.contracts.api;

import java.math.BigDecimal;

public interface Deposit {
    void deposit(String accountId, BigDecimal amount, String currency, String requestId) throws Exception;
}
