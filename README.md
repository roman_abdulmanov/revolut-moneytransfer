Money transfer service for Revolut 
----------------------------------

### Project structure    

```
Root            
├── contracts      contracts between modules
├── core           middleware between REST service and store
├── datastore      store with accounts info
├── distribution   target build
└── server         REST service implementation

```

All project parts implemented as standalone maven modules. 
Main idea here to have ability to switch module implementation without other modules changing.


### Build and Run with Maven 3.6.0 and Java 8
```
mvn clean package
``` 

```
java -jar distribution/target/moneytransfer.jar
```
Alternatively pre-build package can be downloaded from `Downloads` Bitbucket page.
 
Service will be executed on `localhost:8080`

### Implementation Highlights
0. TDD way implementation
1. Ultra-lightweight and thread-safe. Without full synchronized methods, just shared memory locks
2. JSON API with duplicate requests assertion (check `request_id` params and cache with expiration time)
3. DI between modules, low coupling, only public contracts (check `contracts` module) 
4. Minimum dependencies, maximum logs for debugging

### Testing
```
mvn test
```
1. Close to `100%` coverage 
2. All REST API tested by integration tests (check `server` module tests). These tests are slow but unbelievably useful for such kind of checks and development performance    
3. All places with potential concurrency issues covered by multithreaded tests (check `concurrent` folder in `server` module)
 

### API
#### Accounts
##### Get all created accounts:
    [GET] /api/v1/accounts/
`Response`:

    [  
        {  
            "name":"John",
            "id":"861cedc3-479a-421a-afce-93cb963cd431",
            "balance":0,
            "currency":"USD"
        }
    ]
    
##### Get account by id:
    [GET] /api/v1/accounts/{id}
`Response`:

    {  
        "name":"John",
        "id":"861cedc3-479a-421a-afce-93cb963cd431",
        "balance":0,
        "currency":"USD"
    }
  
##### Create new account:  
    [POST] /api/v1/accounts/
    {  
        "name":"John",
        "currency":"USD",
        "request_id":"123456789"
    }
    
`Response`:

    {  
        "name":"John",
        "id":"861cedc3-479a-421a-afce-93cb963cd431",
        "balance":0,
        "currency":"USD"
    }
    
#### Deposit
##### Deposit money to account:
    [POST] /api/v1/deposit/
    {  
       "account_id":"4fc727e0-6b0c-4d34-b8ad-2b64f7830bb3",
       "amount":12.34,
       "currency":"USD",
       "request_id":"123456789"
    }
`Response`: 204 No Content

#### Transfer
##### Transfer money between accounts:
    [POST] /api/v1/transfer/
    {  
       "from_account_id":"4fc727e0-6b0c-4d34-b8ad-2b64f7830bb3",
       "to_account_id":"861cedc3-479a-421a-afce-93cb963cd431",
       "amount":12.34,
       "currency":"USD",
       "request_id":"123456789"
    }
`Response`: 204 No Content

### Libs
| Name   |      Purpose      |
|----------|---------------|
| Log4j |  Logging | 
| JavaMoney|    Money operations   |
| Guava | Cache with expiration time |
| Jetty | HTTP WEB server |
| Jersey | JAX-RS Implementation |
| Jackson | JSON processor |
| Mockito | Tests |
| JUnit | Tests |
| Jersey Test Framework | Tests (for Jersey implementation) |<
| Nitor Creations Matches | Tests (for reflection object comparing) |
| Concurrent Junit | Tests (for multithreaded testing) |


### Some remarks
1. Good to have transaction log
2. Good to remove all hardcoded configuration params (port, cache life) 