package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.models.exceptions.AccountNotFoundException;
import com.revolut.moneystransfer.core.services.models.exceptions.DifferentCurrencyException;
import com.revolut.moneystransfer.core.services.models.exceptions.DuplicateRequestException;
import com.revolut.moneystransfer.core.services.models.exceptions.WrongAmountException;
import com.revolut.moneytransfer.contracts.api.Deposit;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DepositTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private Store store;

    @Mock
    private Account account;

    private Deposit depositService;

    @Before
    public void configure() {
        depositService = new DepositImpl(store);
        when(account.getCurrency()).thenReturn("USD");
    }

    @Test
    public void deposit_requestStoreWithValidParamsForExistingAccount() throws Exception {
        when(store.getAccount("John")).thenReturn(account);

        depositService.deposit("John", BigDecimal.TEN, "USD", "42");
        verify(store, times(1)).deposit("John", BigDecimal.TEN);
    }

    @Test
    public void deposit_throwsForAbsentAccount() throws Exception {
        exception.expect(AccountNotFoundException.class);
        exception.expectMessage("Account not found: John");

        depositService.deposit("John", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void deposit_throwsForWrongCurrency() throws Exception {
        when(store.getAccount("John")).thenReturn(account);
        when(account.getCurrency()).thenReturn("EUR");

        exception.expect(DifferentCurrencyException.class);
        exception.expectMessage("Currency between accounts and operations should be equal");

        depositService.deposit("John", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void deposit_throwsForNegativeAmount() throws Exception {
        when(store.getAccount("John")).thenReturn(account);

        exception.expect(WrongAmountException.class);
        exception.expectMessage("Value should be positive: -13");

        depositService.deposit("John", BigDecimal.valueOf(-13), "USD", "42");
    }

    @Test
    public void deposit_throwsForZeroAmount() throws Exception {
        when(store.getAccount("John")).thenReturn(account);

        exception.expect(WrongAmountException.class);
        exception.expectMessage("Value should be positive: 0");

        depositService.deposit("John", BigDecimal.valueOf(0), "USD", "42");
    }

    @Test
    public void deposit_throwsForCachedRequestId() throws Exception {
        when(store.getAccount("John")).thenReturn(account);

        exception.expect(DuplicateRequestException.class);
        exception.expectMessage("Duplicate request: 42");

        depositService.deposit("John", BigDecimal.TEN, "USD", "42");
        depositService.deposit("John", BigDecimal.TEN, "USD", "42");
    }
}
