package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.models.exceptions.DuplicateRequestException;
import com.revolut.moneytransfer.contracts.api.Accounts;
import com.revolut.moneytransfer.contracts.api.Store;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountsTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private Store store;

    private Accounts accountsService;

    @Before
    public void configure() {
        accountsService = new AccountsImpl(store);
    }

    @Test
    public void getAccounts_requestStoreAccounts() {
        accountsService.getAccounts();
        verify(store, times(1)).getAccounts();
    }

    @Test
    public void getAccount_requestStoreWithAccountId() {
        accountsService.getAccount("John");
        verify(store, times(1)).getAccount("John");
    }

    @Test
    public void createAccount_requestStoreWithValidParams() throws Exception {
        accountsService.createAccount("John", "USD", "42");
        verify(store, times(1)).createAccount("John", "USD");
    }

    @Test
    public void createAccounts_requestStoreWithValidParams() throws Exception {
        accountsService.createAccount("John", "USD", "42");
        accountsService.createAccount("John", "USD", "13");
        verify(store, times(2)).createAccount("John", "USD");
    }

    @Test
    public void createAccount_throwsForCachedRequestId() throws Exception {
        exception.expect(DuplicateRequestException.class);
        exception.expectMessage("Duplicate request: 42");

        accountsService.createAccount("John", "USD", "42");
        accountsService.createAccount("John", "USD", "42");
    }
}
