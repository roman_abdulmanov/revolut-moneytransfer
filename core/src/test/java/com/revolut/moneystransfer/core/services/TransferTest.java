package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.models.exceptions.*;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.api.Transfer;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransferTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private Store store;

    @Mock
    private Account john;

    @Mock
    private Account smith;

    private Transfer transferService;

    @Before
    public void configure() {
        transferService = new TransferImpl(store);
        when(john.getCurrency()).thenReturn("USD");
        when(john.getId()).thenReturn("42");
        when(smith.getCurrency()).thenReturn("USD");
        when(smith.getId()).thenReturn("43");
    }

    @Test
    public void transfer_requestStoreWithValidParamsForExistingAccounts() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
        verify(store, times(1)).transfer("John", "Smith", BigDecimal.TEN);
    }

    @Test
    public void transfer_multiRequestStoreWithValidParamsForExistingAccounts() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "43");
        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "44");
        verify(store, times(3)).transfer("John", "Smith", BigDecimal.TEN);
    }

    @Test
    public void transfer_throwsForAbsentFromAccount() throws Exception {
        exception.expect(AccountNotFoundException.class);
        exception.expectMessage("Account not found: John");

        transferService.transfer("John", "John", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void transfer_throwsForAbsentToAccount() throws Exception {
        when(store.getAccount("John")).thenReturn(john);

        exception.expect(AccountNotFoundException.class);
        exception.expectMessage("Account not found: Smith");

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void transfer_throwsForWrongCurrencyWithFromAccount() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);
        when(john.getCurrency()).thenReturn("EUR");

        exception.expect(DifferentCurrencyException.class);
        exception.expectMessage("Currency between accounts and operations should be equal");

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void transfer_throwsForWrongCurrencyWithToAccount() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);
        when(smith.getCurrency()).thenReturn("EUR");

        exception.expect(DifferentCurrencyException.class);
        exception.expectMessage("Currency between accounts and operations should be equal");

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void transfer_throwsForNegativeAmount() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);

        exception.expect(WrongAmountException.class);
        exception.expectMessage("Value should be positive: -13");

        transferService.transfer("John", "Smith", BigDecimal.valueOf(-13), "USD", "42");
    }

    @Test
    public void transfer_throwsForZeroAmount() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);

        exception.expect(WrongAmountException.class);
        exception.expectMessage("Value should be positive: 0");

        transferService.transfer("John", "Smith", BigDecimal.valueOf(0), "USD", "42");
    }

    @Test
    public void transfer_throwsForSameAmountFromAndTo() throws Exception {
        when(store.getAccount("John")).thenReturn(john);

        exception.expect(SameAccountsException.class);
        exception.expectMessage("From and to accounts should be unique");

        transferService.transfer("John", "John", BigDecimal.TEN, "USD", "42");
    }

    @Test
    public void transfer_throwsForCachedRequestId() throws Exception {
        when(store.getAccount("John")).thenReturn(john);
        when(store.getAccount("Smith")).thenReturn(smith);

        exception.expect(DuplicateRequestException.class);
        exception.expectMessage("Duplicate request: 42");

        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
        transferService.transfer("John", "Smith", BigDecimal.TEN, "USD", "42");
    }
}
