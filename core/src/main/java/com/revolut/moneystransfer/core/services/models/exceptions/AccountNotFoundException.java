package com.revolut.moneystransfer.core.services.models.exceptions;

public class AccountNotFoundException extends Exception {
    final String _accountId;

    public AccountNotFoundException(String accountId) {
        _accountId = accountId;
    }

    @Override
    public String getMessage() {
        return String.format("Account not found: %s", _accountId);
    }
}

