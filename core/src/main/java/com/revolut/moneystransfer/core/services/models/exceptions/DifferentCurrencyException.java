package com.revolut.moneystransfer.core.services.models.exceptions;

public class DifferentCurrencyException extends Exception {

    @Override
    public String getMessage() {
        return String.format("Currency between accounts and operations should be equal");
    }
}
