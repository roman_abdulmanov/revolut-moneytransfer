package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.base.BaseService;
import com.revolut.moneytransfer.contracts.api.Accounts;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.util.List;

public class AccountsImpl extends BaseService implements Accounts {
    private static final Logger logger = LogManager.getLogger(AccountsImpl.class);

    private Store _store;

    @Inject
    public AccountsImpl(Store store) {
        _store = store;
    }

    public List<Account> getAccounts() {
        logger.debug("Requested get accounts");
        return _store.getAccounts();
    }

    public Account getAccount(String id) {
        logger.debug("Requested account: {}", id);
        return _store.getAccount(id);
    }

    public Account createAccount(String name, String currency, String requestId) throws Exception {
        logger.debug("Create account {}, {} {}", name, currency, requestId);

        checkNotCached(requestId);

        return _store.createAccount(name, currency);
    }
}
