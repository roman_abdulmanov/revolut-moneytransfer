package com.revolut.moneystransfer.core.services.models.exceptions;

public class DuplicateRequestException extends Exception {
    final String _id;

    public DuplicateRequestException(String id) {
        _id = id;
    }

    @Override
    public String getMessage() {
        return String.format("Duplicate request: %s", _id);
    }
}
