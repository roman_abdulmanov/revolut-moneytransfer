package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.base.BaseService;
import com.revolut.moneytransfer.contracts.api.Deposit;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.math.BigDecimal;

public class DepositImpl extends BaseService implements Deposit {
    private static final Logger logger = LogManager.getLogger(DepositImpl.class);

    private Store _store;

    @Inject
    public DepositImpl(Store store) {
        _store = store;
    }

    public void deposit(String accountId, BigDecimal amount, String currency, String requestId) throws Exception {
        logger.debug("Requested deposit {} {} {} {}", accountId, amount, currency, requestId);

        final Account account = _store.getAccount(accountId);
        checkAccountExist(accountId, account);
        checkSameCurrency(currency, account);
        checkPositive(amount);
        checkNotCached(requestId);

        _store.deposit(accountId, amount);
    }
}
