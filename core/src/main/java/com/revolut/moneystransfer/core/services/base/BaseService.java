package com.revolut.moneystransfer.core.services.base;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.revolut.moneystransfer.core.services.models.exceptions.*;
import com.revolut.moneytransfer.contracts.models.Account;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public abstract class BaseService {
    private Cache<String, Date> requests;

    protected BaseService() {
        requests = CacheBuilder.newBuilder()
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .build();
    }

    private boolean isCached(String request) {
        if (requests.getIfPresent(request) != null) {
            return true;
        }

        requests.put(request, new Date());
        return false;
    }

    protected void checkPositive(BigDecimal value) throws WrongAmountException {
        if (value.compareTo(BigDecimal.ZERO) <= 0) {
            throw new WrongAmountException(value);
        }
    }

    protected void checkNotCached(String requestId) throws DuplicateRequestException {
        if (isCached(requestId)) {
            throw new DuplicateRequestException(requestId);
        }
    }

    protected void checkSameCurrency(String currency, Account account) throws DifferentCurrencyException {
        if (currency.compareToIgnoreCase(account.getCurrency()) != 0) {
            throw new DifferentCurrencyException();
        }
    }

    protected void checkAccountExist(String accountId, Object account) throws AccountNotFoundException {
        if (account == null) {
            throw new AccountNotFoundException(accountId);
        }
    }

    protected void checkDifferentAccount(Account from, Account to) throws SameAccountsException {
        if (from.getId().equalsIgnoreCase(to.getId())) {
            throw new SameAccountsException();
        }
    }
}
