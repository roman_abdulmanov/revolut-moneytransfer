package com.revolut.moneystransfer.core.services.models.exceptions;

public class SameAccountsException extends Exception {
    @Override
    public String getMessage() {
        return String.format("From and to accounts should be unique");
    }
}
