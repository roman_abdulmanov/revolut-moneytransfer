package com.revolut.moneystransfer.core.services.models.exceptions;

import java.math.BigDecimal;

public class WrongAmountException extends Exception {
    final BigDecimal _value;

    public WrongAmountException(BigDecimal value) {
        _value = value;
    }

    @Override
    public String getMessage() {
        return String.format("Value should be positive: %s", _value);
    }
}

