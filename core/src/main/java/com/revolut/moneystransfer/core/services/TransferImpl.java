package com.revolut.moneystransfer.core.services;

import com.revolut.moneystransfer.core.services.base.BaseService;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.api.Transfer;
import com.revolut.moneytransfer.contracts.models.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import java.math.BigDecimal;

public class TransferImpl extends BaseService implements Transfer {
    private static final Logger logger = LogManager.getLogger(TransferImpl.class);

    private Store _store;

    @Inject
    public TransferImpl(Store store) {
        _store = store;
    }

    public void transfer(String fromAccountId, String toAccountId, BigDecimal amount, String currency, String requestId) throws Exception {
        logger.debug("Requested transfer {} {} {} {} {}", fromAccountId, toAccountId, amount, currency, requestId);

        final Account fromAccount = _store.getAccount(fromAccountId);
        final Account toAccount = _store.getAccount(toAccountId);

        checkAccountExist(fromAccountId, fromAccount);
        checkAccountExist(toAccountId, toAccount);

        checkSameCurrency(currency, fromAccount);
        checkSameCurrency(currency, toAccount);

        checkPositive(amount);
        checkDifferentAccount(fromAccount, toAccount);

        checkNotCached(requestId);

        _store.transfer(fromAccountId, toAccountId, amount);
    }
}
