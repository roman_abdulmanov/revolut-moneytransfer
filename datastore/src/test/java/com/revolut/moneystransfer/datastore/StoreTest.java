package com.revolut.moneystransfer.datastore;

import com.revolut.moneystransfer.datastore.models.exceptions.BalanceNotEnoughException;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.money.UnknownCurrencyException;
import java.math.BigDecimal;
import java.util.List;

import static com.nitorcreations.Matchers.reflectEquals;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class StoreTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private Store store;

    @Before
    public void configure() {
        store = new StoreImpl();
    }

    @Test
    public void createAccount() {
        final Account account = store.createAccount("John", "USD");

        assertThat(account.getId(), is(notNullValue()));
        assertThat(account.getName(), is("John"));
        assertThat(account.getBalance(), is(BigDecimal.ZERO));
        assertThat(account.getCurrency(), is("USD"));
    }

    @Test
    public void getAccount() {
        final Account account = store.createAccount("John", "USD");
        final Account result = store.getAccount(account.getId());

        assertThat(result, reflectEquals(account));
    }

    @Test
    public void getAccounts() {
        final Account john = store.createAccount("John", "USD");
        final Account smith = store.createAccount("Smith", "USD");

        final List<Account> accounts = store.getAccounts();
        assertThat(accounts.size(), is(2));

        final Account first = accounts.stream().filter((a) -> a.getName().equals("John")).findFirst().get();
        final Account second = accounts.stream().filter((a) -> a.getName().equals("Smith")).findFirst().get();

        assertThat(first, reflectEquals(john));
        assertThat(second, reflectEquals(smith));
    }

    @Test
    public void getAccounts_forEmptyStore() {
        final List<Account> accounts = store.getAccounts();
        assertThat(accounts.isEmpty(), is(true));
    }

    @Test
    public void createAccount_generateDifferentIds() {
        final Account john = store.createAccount("John", "USD");
        final Account smith = store.createAccount("Smith", "USD");

        assertThat(john.getId(), is(not((smith.getId()))));
    }

    @Test
    public void createAccount_throwsForWrongCurrency() {
        exception.expect(UnknownCurrencyException.class);
        exception.expectMessage("Unknown currency code: ABC");

        final Account john = store.createAccount("John", "ABC");
    }

    @Test
    public void deposit() throws Exception {
        final Account john = store.createAccount("John", "USD");

        store.deposit(john.getId(), BigDecimal.valueOf(25));

        assertThat(store.getAccount(john.getId()).getBalance(), is(BigDecimal.valueOf(25)));
    }

    @Test
    public void transfer() throws Exception {
        final Account john = store.createAccount("John", "USD");
        final Account smith = store.createAccount("Smith", "USD");

        store.deposit(john.getId(), BigDecimal.valueOf(25));
        store.transfer(john.getId(), smith.getId(), BigDecimal.valueOf(11.5));

        assertThat(store.getAccount(john.getId()).getBalance(), is(BigDecimal.valueOf(13.5)));
        assertThat(store.getAccount(smith.getId()).getBalance(), is(BigDecimal.valueOf(11.5)));
    }

    @Test
    public void transfer_moreThenOnBalance() throws Exception {
        final Account john = store.createAccount("John", "USD");
        final Account smith = store.createAccount("Smith", "USD");

        exception.expect(BalanceNotEnoughException.class);
        exception.expectMessage("Balance not enough");

        store.transfer(john.getId(), smith.getId(), BigDecimal.TEN);
    }
}