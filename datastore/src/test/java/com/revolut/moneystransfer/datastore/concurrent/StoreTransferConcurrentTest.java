package com.revolut.moneystransfer.datastore.concurrent;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.revolut.moneystransfer.datastore.StoreImpl;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(ConcurrentTestRunner.class)
public class StoreTransferConcurrentTest {
    private Account john;
    private Account smith;
    private Store store;

    @Before
    public void configure() throws Exception {
        store = new StoreImpl();
        john = store.createAccount("John", "USD");
        smith = store.createAccount("Smith", "USD");

        store.deposit(john.getId(), BigDecimal.valueOf(200));
    }

    @Test
    @ThreadCount(100)
    public void deposit() throws Exception {
        store.transfer(john.getId(), smith.getId(), BigDecimal.ONE);
    }

    @After
    public void checkAccounts() {
        assertThat(store.getAccount(john.getId()).getBalance(), is(BigDecimal.valueOf(100)));
        assertThat(store.getAccount(smith.getId()).getBalance(), is(BigDecimal.valueOf(100)));
    }
}
