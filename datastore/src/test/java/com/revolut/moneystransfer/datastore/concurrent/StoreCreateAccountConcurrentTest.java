package com.revolut.moneystransfer.datastore.concurrent;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.revolut.moneystransfer.datastore.StoreImpl;
import com.revolut.moneytransfer.contracts.api.Store;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(ConcurrentTestRunner.class)
public class StoreCreateAccountConcurrentTest {
    private Store store;

    @Before
    public void configure() {
        store = new StoreImpl();
    }

    @Test
    @ThreadCount(100)
    public void createAccount() {
        store.createAccount("John", "USD");
    }

    @After
    public void checkAccounts() {
        assertThat(store.getAccounts().size(), is(100));
    }
}
