package com.revolut.moneystransfer.datastore.concurrent;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.revolut.moneystransfer.datastore.StoreImpl;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(ConcurrentTestRunner.class)
public class StoreDepositConcurrentTest {
    private Account account;
    private Store store;

    @Before
    public void configure() {
        store = new StoreImpl();
        account = store.createAccount("John", "USD");
    }

    @Test
    @ThreadCount(100)
    public void deposit() throws Exception {
        store.deposit(account.getId(), BigDecimal.ONE);
    }

    @After
    public void checkBalance() {
        assertThat(store.getAccount(account.getId()).getBalance(), is(BigDecimal.valueOf(100)));
    }
}
