package com.revolut.moneystransfer.datastore.concurrent;

import com.anarsoft.vmlens.concurrent.junit.ConcurrentTestRunner;
import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.revolut.moneystransfer.datastore.StoreImpl;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(ConcurrentTestRunner.class)
public class StoreGetAccountsWithModifyOperationsConcurrentTest {
    private Store store;

    @Before
    public void configure() {
        store = new StoreImpl();
    }

    @Test
    @ThreadCount(100)
    public void getAccounts() throws Exception {
        final Account john = store.createAccount("John", "USD");
        store.deposit(john.getId(), BigDecimal.ONE);

        final Account smith = store.createAccount("Smith", "USD");
        store.transfer(john.getId(), smith.getId(), BigDecimal.ONE);

        store.getAccounts();
    }

    @After
    public void checkAccounts() {
        assertThat(store.getAccounts().size(), is(200));
    }
}
