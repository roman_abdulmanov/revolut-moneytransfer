package com.revolut.moneystransfer.datastore.models.exceptions;

public class BalanceNotEnoughException extends Exception {
    @Override
    public String getMessage() {
        return String.format("Balance not enough");
    }
}
