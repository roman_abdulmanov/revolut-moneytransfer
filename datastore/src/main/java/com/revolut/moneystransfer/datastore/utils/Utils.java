package com.revolut.moneystransfer.datastore.utils;

import com.revolut.moneystransfer.datastore.models.AccountDetails;
import com.revolut.moneytransfer.contracts.models.dto.AccountDto;
import com.revolut.moneytransfer.contracts.models.Account;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

public class Utils {
    public static Account convert(String id, AccountDetails details) {
        return new AccountDto(
                id,
                details.getName(),
                details.getMonetary().getNumber().numberValue(BigDecimal.class),
                details.getMonetary().getCurrency().getCurrencyCode());
    }

    public static String generateId(Map map) {
        String id = null;
        while (id == null || map.containsKey(id)) {
            id = UUID.randomUUID().toString();
        }

        return id;
    }
}
