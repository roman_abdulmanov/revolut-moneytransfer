package com.revolut.moneystransfer.datastore.models;

import javax.money.MonetaryAmount;

public class AccountDetails {
    private String _name;
    private MonetaryAmount _monetary;

    public AccountDetails(String name, MonetaryAmount monetary) {
        _name = name;
        _monetary = monetary;
    }

    public String getName() {
        return _name;
    }

    public MonetaryAmount getMonetary() {
        return _monetary;
    }

    public void setMonetary(MonetaryAmount _monetary) {
        this._monetary = _monetary;
    }
}
