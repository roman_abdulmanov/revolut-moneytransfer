package com.revolut.moneystransfer.datastore;

import com.revolut.moneystransfer.datastore.models.AccountDetails;
import com.revolut.moneystransfer.datastore.models.exceptions.BalanceNotEnoughException;
import com.revolut.moneystransfer.datastore.utils.Utils;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.models.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class StoreImpl implements Store {
    private static final Logger logger = LogManager.getLogger(StoreImpl.class);

    private ConcurrentHashMap<String, AccountDetails> accounts = new ConcurrentHashMap<>();

    public Account createAccount(String name, String currency) {
        final MonetaryAmount monetary = Money.of(0, currency);

        String id;
        while (accounts.putIfAbsent(id = Utils.generateId(accounts), new AccountDetails(name, monetary)) != null) {
            logger.info("Conflict key found {}, regenerate...", id);
        }

        final Account account = Utils.convert(id, get(id));
        logger.debug("Created account {}", account);
        return account;
    }

    public void transfer(String fromId, String toId, BigDecimal amount) throws BalanceNotEnoughException {
        deposit(fromId, amount.negate());
        deposit(toId, amount);

        logger.debug("Transferred from {} to {} amount {}", fromId, toId, amount);
    }

    public void deposit(String id, BigDecimal amount) throws BalanceNotEnoughException {
        final AccountDetails details = get(id);
        synchronized (details) {
            final MonetaryAmount money = details.getMonetary();
            final MonetaryAmount tmp = money.add(Money.of(amount, money.getCurrency()));

            if (tmp.isNegative()) {
                throw new BalanceNotEnoughException();
            }

            details.setMonetary(tmp);
        }

        logger.debug("Deposit for {} amount {}", id, amount);
    }

    public List<Account> getAccounts() {
        return accounts.keySet().stream().map(key -> Utils.convert(key, get(key))).collect(Collectors.toList());
    }

    public Account getAccount(String id) {
        final AccountDetails details = get(id);
        return details != null ? Utils.convert(id, details) : null;
    }

    private AccountDetails get(String id) {
        return accounts.get(id);
    }
}
