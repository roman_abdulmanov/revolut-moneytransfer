package com.revolut.moneytransfer.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.moneytransfer.server.models.base.BaseRequest;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


public class TransferRequest extends BaseRequest {
    @NotNull
    @JsonProperty("from_account_id")
    private String fromAccountId;

    @NotNull
    @JsonProperty("to_account_id")
    private String toAccountId;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private String currency;

    public TransferRequest() {

    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(String fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId) {
        this.toAccountId = toAccountId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
