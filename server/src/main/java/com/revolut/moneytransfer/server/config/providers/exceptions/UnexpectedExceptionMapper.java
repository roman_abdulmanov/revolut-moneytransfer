package com.revolut.moneytransfer.server.config.providers.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Provider
public class UnexpectedExceptionMapper implements ExceptionMapper<Exception> {
    private static final transient ObjectMapper MAPPER = new ObjectMapper();

    public Response toResponse(final Exception exception) {
        ResponseBuilder builder = Response.status(Status.INTERNAL_SERVER_ERROR)
                .entity(defaultJSON(exception))
                .type(MediaType.APPLICATION_JSON);
        return builder.build();
    }

    private String defaultJSON(final Exception exception) {
        try {
            return MAPPER.writeValueAsString(new ErrorInfo(exception.getMessage()));
        } catch (JsonProcessingException e) {
            return "{\"message\":\"An internal error occurred\"}";
        }
    }
}