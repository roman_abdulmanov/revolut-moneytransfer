package com.revolut.moneytransfer.server;

import com.revolut.moneytransfer.server.config.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.debug("Starting app...");

        final ServletHolder jerseyServlet
                = new ServletHolder(new ServletContainer(new AppConfig()));
        final Server server = new Server(8080);
        final ServletContextHandler context
                = new ServletContextHandler(server, "/");
        context.addServlet(jerseyServlet, "/api/v1/*");

        try {
            logger.debug("Trying to start server");
            server.start();
            logger.debug("Trying to join");
            server.join();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            logger.debug("Trying to destroy server");
            server.destroy();
        }
    }
}