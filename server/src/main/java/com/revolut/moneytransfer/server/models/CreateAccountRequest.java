package com.revolut.moneytransfer.server.models;

import com.revolut.moneytransfer.server.models.base.BaseRequest;

import javax.validation.constraints.NotNull;


public class CreateAccountRequest extends BaseRequest {
    @NotNull
    private String name;

    @NotNull
    private String currency;

    public CreateAccountRequest() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
