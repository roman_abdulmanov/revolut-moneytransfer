package com.revolut.moneytransfer.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.moneytransfer.server.models.base.BaseRequest;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


public class DepositRequest extends BaseRequest {
    @NotNull
    @JsonProperty("account_id")
    private String accountId;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private String currency;

    public DepositRequest() {

    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
