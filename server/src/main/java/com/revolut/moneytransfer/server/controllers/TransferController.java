package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.api.Transfer;
import com.revolut.moneytransfer.server.models.TransferRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("transfer")
public class TransferController {
    private static final Logger logger = LogManager.getLogger(TransferController.class);

    private Transfer _transfer;

    @Inject
    TransferController(Transfer transfer) {
        _transfer = transfer;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void transfer(@Valid TransferRequest request) throws Exception {

        logger.debug("Transfer accounts request {} {} {} {} {}",
                request.getFromAccountId(),
                request.getToAccountId(),
                request.getAmount(),
                request.getCurrency(),
                request.getRequestId());

        _transfer.transfer(
                request.getFromAccountId(),
                request.getToAccountId(),
                request.getAmount(),
                request.getCurrency(),
                request.getRequestId());
    }
}
