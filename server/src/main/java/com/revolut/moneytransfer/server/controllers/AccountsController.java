package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.api.Accounts;
import com.revolut.moneytransfer.contracts.models.Account;
import com.revolut.moneytransfer.server.models.CreateAccountRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("accounts")
public class AccountsController {
    private static final Logger logger = LogManager.getLogger(AccountsController.class);

    private Accounts _accounts;

    @Inject
    AccountsController(Accounts accounts) {
        _accounts = accounts;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Account> getAccounts() {
        logger.debug("Get accounts request");
        return _accounts.getAccounts();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(
            @NotNull @PathParam("id") String id) {
        logger.debug("Get account {} request", id);
        final Account account = _accounts.getAccount(id);
        return account != null ?
                Response.ok(account).build() :
                Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Account createAccount(@Valid CreateAccountRequest request) throws Exception {

        logger.debug("Create account {} {} {} request", request.getName(), request.getCurrency(), request.getRequestId());
        return _accounts.createAccount(request.getName(), request.getCurrency(), request.getRequestId());
    }
}