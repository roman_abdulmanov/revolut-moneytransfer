package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.api.Deposit;
import com.revolut.moneytransfer.server.models.DepositRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("deposit")
public class DepositController {
    private static final Logger logger = LogManager.getLogger(TransferController.class);

    private Deposit _deposit;

    @Inject
    DepositController(Deposit deposit) {
        _deposit = deposit;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void deposit(@Valid DepositRequest request) throws Exception {

        logger.debug("Deposit request {} {} {} {}", request.getAccountId(), request.getAmount(), request.getCurrency(), request.getRequestId());
        _deposit.deposit(request.getAccountId(), request.getAmount(), request.getCurrency(), request.getRequestId());
    }
}