package com.revolut.moneytransfer.server.config;

import com.revolut.moneystransfer.core.services.AccountsImpl;
import com.revolut.moneystransfer.core.services.DepositImpl;
import com.revolut.moneystransfer.core.services.TransferImpl;
import com.revolut.moneystransfer.datastore.StoreImpl;
import com.revolut.moneytransfer.contracts.api.Accounts;
import com.revolut.moneytransfer.contracts.api.Deposit;
import com.revolut.moneytransfer.contracts.api.Store;
import com.revolut.moneytransfer.contracts.api.Transfer;
import com.revolut.moneytransfer.server.config.providers.exceptions.UnexpectedExceptionMapper;
import com.revolut.moneytransfer.server.controllers.AccountsController;
import com.revolut.moneytransfer.server.controllers.DepositController;
import com.revolut.moneytransfer.server.controllers.TransferController;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class AppConfig extends ResourceConfig {
    public AppConfig() {
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(StoreImpl.class).to(Store.class).in(Singleton.class);
                bind(AccountsImpl.class).to(Accounts.class).in(Singleton.class);
                bind(TransferImpl.class).to(Transfer.class).in(Singleton.class);
                bind(DepositImpl.class).to(Deposit.class).in(Singleton.class);
            }
        });

        register(AccountsController.class);
        register(TransferController.class);
        register(DepositController.class);

        register(new UnexpectedExceptionMapper());
    }
}
