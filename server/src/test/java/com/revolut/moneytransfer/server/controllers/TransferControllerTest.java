package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.models.Account;
import com.revolut.moneytransfer.server.controllers.base.BaseControllerTest;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TransferControllerTest extends BaseControllerTest {
    @Test
    public void transferPostRequestForValidAccounts_returnOkResponse() throws IOException {
        final Account john = createAccount("John", "USD");
        final Account smith = createAccount("Smith", "USD");
        deposit(john.getId(), john.getCurrency(), BigDecimal.TEN);
        transfer(john.getId(), smith.getId(), "USD", BigDecimal.valueOf(3.35));
        transfer(smith.getId(), john.getId(), "USD", BigDecimal.valueOf(1));

        assertThat(getAccount(john.getId()).getBalance(), is(BigDecimal.valueOf(7.65)));
        assertThat(getAccount(smith.getId()).getBalance(), is(BigDecimal.valueOf(2.35)));
    }

    @Test
    public void transferPostRequestWithoutNameParam_returnBadRequestResponse() {
        final Response firstResponse = transferResponse(null, "");
        assertResponseEquals(BAD_REQUEST, firstResponse);

        final Response secondResponse = transferResponse("", null);
        assertResponseEquals(BAD_REQUEST, secondResponse);
    }

    @Test
    public void transferPostRequestWithoutCurrencyParam_returnBadRequestResponse() {
        final Response response = transferResponse("John", "Smith", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void transferPostRequestWithoutAmountParam_returnBadRequestResponse() {
        final Response response = transferResponse("John", "Smith", "USD", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void transferPostRequestWithoutRequestIdParam_returnBadRequestResponse() {
        final Response response = transferResponse("John", "Smith", "USD", BigDecimal.TEN, null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void transferPostRequestForInvalidAccount_returnServerErrorResponse() {
        final Response response = depositResponse("42");
        assertResponseEquals(INTERNAL_SERVER_ERROR, response);
    }
}
