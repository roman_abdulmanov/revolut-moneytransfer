package com.revolut.moneytransfer.server.controllers.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.moneytransfer.contracts.models.Account;
import com.revolut.moneytransfer.contracts.models.dto.AccountDto;
import com.revolut.moneytransfer.server.config.AppConfig;
import com.revolut.moneytransfer.server.models.CreateAccountRequest;
import com.revolut.moneytransfer.server.models.DepositRequest;
import com.revolut.moneytransfer.server.models.TransferRequest;
import org.glassfish.jersey.test.JerseyTest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public abstract class BaseControllerTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new AppConfig();
    }

    protected void assertResponseEquals(Response.Status status, Response response) {
        assertThat(response.getStatus(), is(status.getStatusCode()));
    }

    protected void assertResponseEquals(Response.Status status, Response response, String message) throws IOException {
        assertThat(getBody(response), is(message));
        assertResponseEquals(status, response);
    }

    protected void assertAccount(Account account) {
        assertAccount(account, "John", "USD", BigDecimal.ZERO);
    }

    protected void assertAccount(Account account, String name, String currency) {
        assertAccount(account, name, currency, BigDecimal.ZERO);
    }

    protected AccountDto createAccount(String name, String currency) throws IOException {
        final Response response = createAccountResponse(name, currency);

        assertResponseEquals(OK, response);

        return toAccountDto(response);
    }

    protected Response createAccountResponse(String name) {
        return createAccountResponse(name, "USD");
    }

    protected Response createAccountResponse(String name, String currency) {
        return createAccountResponse(name, currency, UUID.randomUUID().toString());
    }

    protected Response createAccountResponse(String name, String currency, String requestId) {
        final CreateAccountRequest request = new CreateAccountRequest();
        request.setName(name);
        request.setCurrency(currency);
        request.setRequestId(requestId);

        return target("/accounts").request()
                .post(Entity.json(request));
    }

    protected void deposit(String account, String currency, BigDecimal amount) {
        deposit(account, currency, amount, UUID.randomUUID().toString());
    }

    private void deposit(String account, String currency, BigDecimal amount, String requestId) {
        final Response response = depositResponse(account, currency, amount, requestId);

        assertResponseEquals(NO_CONTENT, response);
    }

    protected Response depositResponse(String account) {
        return depositResponse(account, "USD");
    }

    protected Response depositResponse(String account, String currency) {
        return depositResponse(account, currency, BigDecimal.TEN);
    }

    protected Response depositResponse(String account, String currency, BigDecimal amount) {
        return depositResponse(account, currency, amount, UUID.randomUUID().toString());
    }

    protected Response depositResponse(String account, String currency, BigDecimal amount, String requestId) {
        final DepositRequest request = new DepositRequest();
        request.setAccountId(account);
        request.setCurrency(currency);
        request.setAmount(amount);
        request.setRequestId(requestId);

        return target("/deposit").request()
                .post(Entity.json(request));
    }

    protected void transfer(String fromAccount, String toAccount, String currency, BigDecimal amount) {
        transfer(fromAccount, toAccount, currency, amount, UUID.randomUUID().toString());
    }

    private void transfer(String fromAccount, String toAccount, String currency, BigDecimal amount, String requestId) {
        final Response response = transferResponse(fromAccount, toAccount, currency, amount, requestId);

        assertResponseEquals(NO_CONTENT, response);
    }

    protected Response transferResponse(String fromAccount, String toAccount) {
        return transferResponse(fromAccount, toAccount, "USD");
    }

    protected Response transferResponse(String fromAccount, String toAccount, String currency) {
        return transferResponse(fromAccount, toAccount, currency, BigDecimal.TEN);
    }

    protected Response transferResponse(String fromAccount, String toAccount, String currency, BigDecimal amount) {
        return transferResponse(fromAccount, toAccount, currency, amount, UUID.randomUUID().toString());
    }

    protected Response transferResponse(String fromAccount, String toAccount, String currency, BigDecimal amount, String requestId) {
        final TransferRequest request = new TransferRequest();
        request.setFromAccountId(fromAccount);
        request.setToAccountId(toAccount);
        request.setCurrency(currency);
        request.setAmount(amount);
        request.setRequestId(requestId);

        return target("/transfer").request()
                .post(Entity.json(request));
    }

    protected AccountDto getAccount(String id) throws IOException {
        final Response response = getAccountResponse(id);

        assertResponseEquals(OK, response);

        return toAccountDto(response);
    }

    protected AccountDto[] getAccounts() throws IOException {
        final Response response = target("/accounts").request()
                .get();

        assertResponseEquals(OK, response);

        return toAccountDtoArray(response);
    }

    protected Response getAccountResponse(String id) {
        return target(String.format("/accounts/%s", id)).request()
                .get();
    }

    private String getBody(Response response) throws IOException {
        return response.readEntity(String.class);
    }

    private void assertAccount(Account account, String name, String currency, BigDecimal amount) {
        assertThat(account.getName(), is(name));
        assertThat(account.getCurrency(), is(currency));
        assertThat(account.getBalance(), is(amount));
        assertThat(account.getId(), is(not(nullValue())));
    }

    private AccountDto toAccountDto(Response response) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(getBody(response), AccountDto.class);
    }

    private AccountDto[] toAccountDtoArray(Response response) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(getBody(response), AccountDto[].class);
    }
}
