package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.models.Account;
import com.revolut.moneytransfer.server.controllers.base.BaseControllerTest;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;

import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class AccountsControllerTest extends BaseControllerTest {
    @Test
    public void accountsGetRequest_returnResponseWithEmptyListForEmptyStore() throws IOException {
        assertThat(getAccounts().length, is(0));
    }

    @Test
    public void accountsGetRequest_returnResponseWithNotEmptyListForNotEmptyStore() throws IOException {
        createAccount("John", "USD");

        final Account[] accounts = getAccounts();
        assertThat(accounts, is(not(nullValue())));
        assertThat(accounts.length, is(1));

        final Account account = accounts[0];
        assertAccount(account);
    }

    @Test
    public void accountsPostRequest_returnResponseWithNotEmptyListForMultiValueStore() throws IOException {
        createAccount("John", "USD");
        createAccount("Smith", "EUR");

        final Account[] accounts = getAccounts();
        assertThat(accounts, is(not(nullValue())));
        assertThat(accounts.length, is(2));

        final Account first = Arrays.stream(accounts).filter((a) -> a.getName().equals("John")).findFirst().get();
        assertAccount(first);

        final Account second = Arrays.stream(accounts).filter((a) -> a.getName().equals("Smith")).findFirst().get();
        assertAccount(second, "Smith", "EUR");
    }

    @Test
    public void accountsPostRequestWithoutNameParam_returnBadRequestResponse() {
        final Response response = createAccountResponse(null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void accountsPostRequestWithoutCurrencyParam_returnBadRequestResponse() {
        final Response response = createAccountResponse("John", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void accountsPostRequestWithoutRequestIdParam_returnBadRequestResponse() {
        final Response response = createAccountResponse("John", "USD", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void accountGetRequest_returnNotFoundResponseForEmptyStore() {
        final Response response = getAccountResponse("42");
        assertResponseEquals(NOT_FOUND, response);
    }

    @Test
    public void accountGetRequest_returnResponseWithAccountForNotEmptyStore() throws IOException {
        final Account account = createAccount("John", "USD");
        assertAccount(getAccount(account.getId()));
    }

    @Test
    public void accountsPostRequestWithoutDuplicateRequestId_returnCustomErrorResponse() throws IOException {
        createAccountResponse("John", "USD", "42");
        final Response response = createAccountResponse("John", "USD", "42");
        assertResponseEquals(INTERNAL_SERVER_ERROR, response, "{\"message\":\"Duplicate request: 42\"}");
    }
}
