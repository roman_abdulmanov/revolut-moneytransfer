package com.revolut.moneytransfer.server.controllers;

import com.revolut.moneytransfer.contracts.models.Account;
import com.revolut.moneytransfer.server.controllers.base.BaseControllerTest;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DepositControllerTest extends BaseControllerTest {
    @Test
    public void depositPostRequestForValidAccount_returnOkResponse() throws IOException {
        final Account account = createAccount("John", "USD");
        deposit(account.getId(), account.getCurrency(), BigDecimal.valueOf(3.35));
        assertThat(getAccount(account.getId()).getBalance(), is(BigDecimal.valueOf(3.35)));
    }

    @Test
    public void depositPostRequestForValidAccountWithBalance_returnOkResponse() throws IOException {
        final Account account = createAccount("John", "USD");
        deposit(account.getId(), account.getCurrency(), BigDecimal.valueOf(3.35));
        deposit(account.getId(), account.getCurrency(), BigDecimal.valueOf(3.35));
        deposit(account.getId(), account.getCurrency(), BigDecimal.valueOf(3.35));
        assertThat(getAccount(account.getId()).getBalance(), is(BigDecimal.valueOf(10.05)));
    }

    @Test
    public void depositPostRequestWithoutNameParam_returnBadRequestResponse() {
        final Response response = depositResponse(null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void depositPostRequestWithoutCurrencyParam_returnBadRequestResponse() {
        final Response response = depositResponse("John", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void depositPostRequestWithoutAmountParam_returnBadRequestResponse() {
        final Response response = depositResponse("John", "USD", null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void depositPostRequestWithoutRequestIdParam_returnBadRequestResponse() {
        final Response response = depositResponse("John", "USD", BigDecimal.TEN, null);
        assertResponseEquals(BAD_REQUEST, response);
    }

    @Test
    public void depositPostRequestForInvalidAccount_returnServerErrorResponse() {
        final Response response = depositResponse("42");
        assertResponseEquals(INTERNAL_SERVER_ERROR, response);
    }
}
